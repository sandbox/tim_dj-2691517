This modules merges your cart created as anonymous with an already existing cart connected to your user account after login.

https://www.drupal.org/sandbox/tim_dj/2691517


Thanks to:
https://gist.github.com/MaffooBristol/65f8057f8ff44c28c768
and
http://jasonrichardsmith.org/blog/drupal-commerce-merge-anonymous-carts-when-logging
